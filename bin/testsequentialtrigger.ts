#!/usr/bin/env node
import * as cdk from 'aws-cdk-lib';
import { TestsequentialtriggerStack } from '../lib/testsequentialtrigger-stack';

const app = new cdk.App();
new TestsequentialtriggerStack(app, 'TestsequentialtriggerStack');
